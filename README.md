Chefsroom docker environment
============================

### Development

This project relies on Docker containers to bootstrap the development environment.

In order to get started, make a local copy the Docker Compose template and spin up the containers.

```bash
$ cp etc/docker/development/docker-compose.yml.dist docker-compose.yml
$ docker-compose up -d
```

Any small tweaks to the development environment must go to your local copy, ```docker-compose.yml```.
This file is excluded from the Git repository.

### Development
