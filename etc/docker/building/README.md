Build environment
=================

```
$ cd api

$ rsync -aqv . etc/docker/building/fpm/app --exclude-from=etc/docker/building/context_excludes.txt
$ rsync -aqv . etc/docker/building/nginx/app --exclude-from=etc/docker/building/context_excludes.txt
$ rsync -aqv . etc/docker/staging/fpm/app --exclude-from=etc/docker/staging/context_excludes.txt
$ rsync -aqv . etc/docker/staging/nginx/app --exclude-from=etc/docker/staging/context_excludes.txt

$ GIT_COMMIT=[sha1 of last commit] docker-compose -f etc/docker/docker-compose-staging.yml build
```

This setup is meant to be used only by the Jenkins CI server.
