Staging environment
===================

```
$ cd api
$ docker-compose up -d -f etc/docker/staging/staging.yml
```

This setup is meant to be used in the staging server.
