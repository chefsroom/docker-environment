/.git
/.idea
/app/config/parameters.yml
/app/config/bootstrap.php.cache
/conf
/etc
/src
/var/cache/*
/var/logs/*
/var/sessions/*
/vendor
/tests
/web/app_dev.php
